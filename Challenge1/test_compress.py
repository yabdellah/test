import unittest
from compress import compress_string

class TestCompress(unittest.TestCase):
    def test_compress(self):
        # test compression operation
        self.assertEqual(compress_string('bbcceeee'),'b2c2e4')
        self.assertEqual(compress_string('aaabbbcccaaa'),'a3b3c3a3')
        self.assertEqual(compress_string('a'),'a')