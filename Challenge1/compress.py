def compress_string(mstr):

    list_c=[]          
    count=1                #count occurence number
    for i in range(len(mstr)):
        if i==0:
            list_c.append(mstr[i])
            continue
        if mstr[i] == mstr[i-1]:
            count=count+1
            if i == len(mstr)-1:
                list_c.append(str(count))
        else:
            if count>1:
                list_c.append(str(count))
            list_c.append(mstr[i])
            count=1
    return "".join(list_c)

print(compress_string("a"))