from django.contrib import admin
from django.urls import path, include

from django_registration.backends.one_step.views import RegistrationView
from users.forms import UserForm
from subscription.views import checkout, create_sub, complete, cancel
from users.views import profile

urlpatterns = [
    path("",profile, name="landing"),
    path("checkout", checkout, name="checkout"),
    path("create-sub", create_sub, name="create sub"), #add
    path("complete", complete, name="complete"), #add
    path("cancel", cancel, name="cancel"), #add this
    path('admin/', admin.site.urls),
    path('register/',
         RegistrationView.as_view(
             form_class=UserForm
         ),
         name='django_registration_register',
    ),
    path('accounts/', include('django_registration.backends.one_step.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
    path('accounts/',  include('users.urls'))
]
