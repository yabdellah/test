def identify_router(ml):
    mdict = {}    # dict to register nodes
    max=0
    mel = []        # contain response 
    for i in range(len(ml)):
        if i==0:
            mdict[ml[i]] = 1
        else:
            if ml[i] in mdict:
                if i == len(ml)-1:
                    mdict[ml[i]]=mdict[ml[i]]+1
                else:
                    mdict[ml[i]]=mdict[ml[i]]+2
            else:
                if i == len(ml)-1:
                    mdict[ml[i]]=1
                else:
                    mdict[ml[i]]=2
        if mdict[ml[i]] > max:
            max = mdict[ml[i]]
        
    for el in mdict:
        if mdict[el]==max:
            mel.append(el)
    return mel

print(identify_router([1,2,3,5,2,1]))