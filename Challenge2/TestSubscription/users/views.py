from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def profile(request):
    
    if request.user.customer == None:
        status = "inactive"
    else:
        status = "active"
    return render(request, 'django_registration/profile.html', {"status": status})

