import unittest
from router import identify_router

class TestRouter(unittest.TestCase):
    def test_router(self):
        # test compression operation
        self.assertEqual(identify_router([1,2,3,5,2,1]),[2])
        self.assertEqual(identify_router([1,3,5,6,4,5,2,6]),[5])
        self.assertEqual(sorted(identify_router([2,4,6,2,5,6])),sorted([6,2]))